let inp;
let array = [];
let error = false;

function setup() {
    createCanvas(1000, 600);
    inp = createInput();
    button = createButton("Adauga");
    button.mousePressed(add);
    button2 = createButton("Random");
    button2.mousePressed(randomizer);
    button3 = createButton("Sort");
    button3.mousePressed(sortare);
}

function draw() {
    background("lightblue");
    if (error) {
        textSize(40);
        fill("black");
        text("Error, try again.", 20, 50);
    }
    drawLines();
}

function add() {
    if (inp.value() <= 300 && inp.value() > 0 && array.length < 30) {
        array.push(Number(inp.value()));
        error = false;
    } else {
        error = true;
    }
}

function drawLines() {
    for (var i = 0; i <= array.length; i++) {
        fill("#00FF00");
        rect(50 + i * 30, 550, 7, -array[i]);
        fill("black");
        text(array[i], 47 + i * 30, 580);
    }
}

function randomizer() {
    error = false;
    array = [];
    for (var i = 0; i <= 30; i++) {
        var number = Math.floor(random(1, 300));
        array.push(number);
    }
}

function mergeSort(array) {
    if (array.length < 2) {
        return array;
    }
    let middle = parseInt(array.length / 2);
    let left = array.slice(0, middle);
    let right = array.slice(middle, array.length);
    return merge(mergeSort(left), mergeSort(right));
}

function merge(left, right) {
    let sortedArray = [];
    while (left.length && right.length) {
        if (left[0] <= right[0]) {
            sortedArray.push(left.shift());
        } else {
            sortedArray.push(right.shift());
        }
    }
    while (left.length) {
        sortedArray.push(left.shift());
    }
    while (right.length) {
        sortedArray.push(right.shift());
    }
    return sortedArray;
}

function sortare() {
    array = mergeSort(array);
}